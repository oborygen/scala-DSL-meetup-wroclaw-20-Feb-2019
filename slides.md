---

# Czego potrzebujesz by zbudować swój własny DSL w Scali!

---

*The basic idea of a domain specific language is a computer language that's targeted to a particular kind of problem, rather than a general purpose language that's aimed at any kind of software problem.*

***Martin Fawler***

---

# Nasze narzędzia

![](https://gitlab.com/oborygen/scala-DSL-meetup-wroclaw-20-Feb-2019/raw/initial/assets/img/tools-498202_1920.jpg){.background}

---

# Nasze narzędzia

- Case Class
- Extension methods
- Implicit conversion
- Tagged types
- Type Class
- Semigroup
- Phantom types
- Shapeless

---

# Przypadek 1 - operacje na wartościach pieniężnych

Przydałby się język do spójnego operowania na wartościach wyrażanych w określonej walucie.

Cele:

- bezpieczeństwo typów - nie mylenie dolarów z euro
- zwięzłe operacje
- kontrola zaokrągleń

---

```scala
trait Currency
case object GBP
case object EUR
case class Money(amount: BigDecimal, currency: Currency)

```

---

# Deklaracja wartości

```scala
val amount = Money(5, GBP)
```

... prawie dobrze

---

# Dodawanie wartości

```scala
val amount1 = Money(10, PLN)
val amount2 = Money(20, EUR)

val total = amount1.copy(amount1.amount + amount2.amount)
```

---

# Lepiej było zostać przy Javie

![](https://gitlab.com/oborygen/scala-DSL-meetup-wroclaw-20-Feb-2019/raw/initial/assets/img/cat-puke.jpg)

---

# Nasz ideał

```scala

val amount1 = 10.PLN
val amount2 = 20.PLN
val amount3 = 1.USD

val total = amount1 + amount2
// Money(30, PLN)

val total = amount1 + amount3
// compilation error

```

---

# Przypadek 1
## Podejscie 2.

```scala
object Money {
  object syntax {

    implicit class IntOps(value: Int) {
      def apply(currency: Currency) = Money(value, currency)
    }

    implicit val moneySemigroup =
      Semigroup.instance[Money]((amount1, amount2) =>
        amount1.copy(amount1.amount + amount2.amount))

  }
}
```
---

# Test

```scala
val amount1 = 10(GBP)
val amount2 = 20(GBP)

val total = amount1 |+| amount2
//amount3: com.oborygen.dsl.money.Money = Money(30,GBP)
```

... lepiej

---

# Czy można dodawać wartości o różnych walutach?

```scala
val amount1 = 10(GBP)
val amount3 = 4(EUR)

val total = amount1 |+| amount3
// total1: com.oborygen.dsl.money.Money = Money(14,GBP)
```

---

# Można

![](http://justsomething.co/wp-content/uploads/2018/12/15-disappointed-cats-who-are-judging-you-poor-life-choices-33.jpg)

---

# Przypadek 1

## Podejście 3

```scala
import shapeless.tag, shapeless.tag.@@

type Money[CUR] = BigDecimal @@ CUR

trait Currency
trait PLN extends Currency
trait GBP extends Currency
trait EUR extends Currency
trait USD extends Currency
```

---

```scala
import cats.syntax.semigroup._, cats.kernel.Semigroup
import shapeless.tag, shapeless.tag.@@

implicit def moneySemigroup[C]: Semigroup[Money[C]] =
Semigroup.instance[Money[C]]((amount1, amount2) =>
  tag[C][BigDecimal](amount1.bigDecimal.add(amount2.bigDecimal)))


val amount1: Money[USD] = tag[USD][BigDecimal](10)
val amount2: Money[USD] = tag[USD][BigDecimal](20)
val amount3: Money[EUR] = tag[EUR][BigDecimal](5)
```

---

```scala
val total1 = amount1 |+| amount2
// total1: Money[USD] = 30

val total2 = amount1 |+| amount3
// error: type mismatch; expected BigDecimal @@ USD actual BigDecimal @@ EUR
```

... no już prawie dobrze

---

# Ostatnie szlify

```scala
implicit class CurrencyOps[N](value: N)(implicit conv: N => BigDecimal) {
    def usd: BigDecimal @@ USD = tag[USD][BigDecimal](conv(value))
    def eur: BigDecimal @@ EUR = tag[EUR][BigDecimal](conv(value))
    def pln: BigDecimal @@ PLN = tag[PLN][BigDecimal](conv(value))
}

import scala.language.postfixOps

val amount1: Money[USD] = 10 usd
val amount2: Money[USD] = 20.4 usd
val amount3: Money[EUR] = 5.4 eur
```

---

![](https://gitlab.com/oborygen/scala-DSL-meetup-wroclaw-20-Feb-2019/raw/initial/assets/img/smiling-cat.jpg)

---

# Żeby nie było zbyt różowo

- waluty uwięzione są jedynie w typie
- pełną sygnaturę typu trzeba będzie przekazywać przez wszystkie warsty obliczeń

---

# Użyte narzedzia

- Case class
- Extension methods
- Type class (Semigroup oraz konwersja na BigDecimal)

---

# Przypadek 2.

## Object builder

Buildery to dość ciekawy obszar dla zagadnień DSL.

Posłużmy się przykładem z mojego podwórka - **promocje**.

{.column}

## Atrybuty promocji

- określone produkty
- zniżka procentowa lub w walucie
- opcjonalnie darmowa dostawa

---

## Domena

```scala
  case class Promotion(id: String, products: Set[String], reward: Discount, freeDelivery: Boolean)

  trait Discount
  case class Cash(amount: BigDecimal) extends Discount
  case class Percent(percent: Int) extends Discount
```

---

# Nasz język

```
promotion id "id" on "product1" and "product2" discounts X.percent
```

Taki cukier syntaktycznego, który formalnie rozwija się do:

```scala
promotion.id("id").on("product1").and("product2").discounts(X.percent)
```

---

# Czego użyłem?

- Extension method
- Phantom types
- Infix notation

---

# Co siedzi pod spodem
## Określenie stanu buildera 

```scala
trait IdDefined
trait ProductsDefined
trait DiscountDefined
trait DeliveryDefined

trait Yes extends IdDefined with ProductsDefined with DiscountDefined with DeliveryDefined
trait No extends IdDefined with ProductsDefined with DiscountDefined with DeliveryDefined
```

---

# Co siedzi pod spodem
## Builder

```scala
    trait Builder[
      ID <: IdDefined,
      PR <: ProductsDefined,
      DI <: DiscountDefined,
      DE <: DeliveryDefined] {
        ...
      }
```

---

# Co siedzi pod spodem
## Określenie ID promocji = zmiana stanu buildera

```scala
      @implicitNotFound("ID has been already defined")
      def id(id: String)(implicit idEv: ID =:= No): Builder[Yes, No, No, No] = {
        ???
      }
```

---

# Anatomia phantom type

```scala
trait Builder[T] {
  def op(implicit ev: T =:= Expected) = ???
}
```

{.column}

Czym jest `=:=`

```scala
object =:= {
     implicit def tpEquals[A]: A =:= A = singleton_=:=.asInstanceOf[A =:= A]
  }
```

---

# Przykłady z życia

```scala
List("a" -> "b").toMap
res0: scala.collection.immutable.Map[String,String] = Map(a -> b)

List("a", "b").toMap
<console>:12: error: Cannot prove that String <:< (T, U).
       List("a", "b").toMap

```

---

# Przykład 3 - query

```scala
  trait Predicate
  case class Query(table: Symbol, fields: List[Symbol], filters: List[Predicate])
```

---

# Przykład 3 - DSL

```scala
Query table 'abc select 'name
```

---

# Przykład 3 - DSL od kuchni

```scala
object Query {
  def table(table: Symbol): Query = Query(table, List.empty, List.empty)
}

implicit class QueryOps(query: Query) {
  def select(field: Symbol) = query.copy(fields = query.fields :+ field)
}
```

---

# Inne mechanizmy
## To następnym razem :)

* Path dependent types
* Shapeless

---

# Dziękuję za uwagę {.big}
