# Slides compilation

For command line use, install `md2gslides` globally:

```bash
npm install -g md2gslides
```

After installing, import your slides by running:

```bash
md2gslides slides.md
```

The first time the command is run you will be prompted for authorization. Credentials will be stored locally in a file named `~/.credentials/md2gslides.json`.
