package com.oborygen.dsl.money

object Samples2 {

  trait Currency

  case object PLN extends Currency

  case object GBP extends Currency

  case object EUR extends Currency

  case object USD extends Currency

  case class Money(amount: BigDecimal, currency: Currency)

  implicit class IntOps(value: Int) {
    def apply(currency: Currency) = Money(value, currency)
  }

  import cats.kernel.Semigroup

  implicit val moneySemigroup: Semigroup[Money] =
    Semigroup.instance[Money]((amount1, amount2) =>
      amount1.copy(amount1.amount + amount2.amount))

  import cats.implicits._

  val amount1 = 10(GBP)
  val amount2 = 20(GBP)

  val total = amount1 |+| amount2

  val amount3 = 4(EUR)

  val total1 = amount1 |+| amount3
}
