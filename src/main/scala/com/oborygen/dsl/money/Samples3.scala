package com.oborygen.dsl.money

import scala.language.postfixOps

object Samples3 {

  import cats.syntax.semigroup._
  import cats.kernel.Semigroup
  import shapeless.tag
  import shapeless.tag.@@

  type Money[CUR] = BigDecimal @@ CUR

  trait Currency
  trait PLN extends Currency
  trait GBP extends Currency
  trait EUR extends Currency
  trait USD extends Currency

  implicit class CurrencyOps[N](value: N)(implicit conv: N => BigDecimal) {
    def usd: BigDecimal @@ USD = tag[USD][BigDecimal](conv(value))
    def eur: BigDecimal @@ EUR = tag[EUR][BigDecimal](conv(value))
    def pln: BigDecimal @@ PLN = tag[PLN][BigDecimal](conv(value))
  }

  implicit def moneySemigroup[C]: Semigroup[Money[C]] =
    Semigroup.instance[Money[C]]((amount1, amount2) =>
      tag[C][BigDecimal](amount1.bigDecimal.add(amount2.bigDecimal)))


  val amount1: Money[USD] = 10 usd
  val amount2: Money[USD] = 20.4 usd
  val amount3: Money[EUR] = 5.4 eur

  val total1 = amount1 |+| amount2

//  val total2 = amount1 |+| amount3
}
