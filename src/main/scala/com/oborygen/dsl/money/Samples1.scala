package com.oborygen.dsl.money

class Samples1 {

  trait Currency

  case object PLN extends Currency

  case object GBP extends Currency

  case object EUR extends Currency

  case object USD extends Currency

  case class Money(amount: BigDecimal, currency: Currency)

  val amount1 = Money(10, GBP)
  val amount2 = Money(20, GBP)

  val amount3 = amount1.copy(amount1.amount + amount2.amount)

}
