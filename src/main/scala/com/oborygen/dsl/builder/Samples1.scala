package com.oborygen.dsl.builder

import scala.annotation.implicitNotFound
import scala.language.postfixOps

object Samples1 {

  case class Promotion(id: String, products: Set[String], reward: Discount, freeDelivery: Boolean)

  trait Discount
  case class Cash(amount: BigDecimal) extends Discount
  case class Percent(percent: Int) extends Discount

  // promotion id "id" on "1" and "2" and "3" discounts 10 cash
  // promotion id "id" on "1" and "2" and "3" discounts 20 percent

  val sum = List("a" -> "b").toMap

  object syntax {

    trait IdDefined
    trait ProductsDefined
    trait DiscountDefined
    trait DeliveryDefined

    trait Yes extends IdDefined with ProductsDefined with DiscountDefined with DeliveryDefined
    trait No extends IdDefined with ProductsDefined with DiscountDefined with DeliveryDefined

    class Percent(val value: Int) extends AnyVal
    class Cash(val value: BigDecimal) extends AnyVal

    implicit class IntOps(value: Int) {
      def percent = new Percent(value)
    }
    implicit class BigDecimalOps(value: BigDecimal) {
      def cash = new Cash(value)
    }

    trait Builder[ID <: IdDefined, PR <: ProductsDefined, DI <: DiscountDefined, DE <: DeliveryDefined] {

      @implicitNotFound("ID has been already defined")
      def id(id: String)(implicit idEv: ID =:= No): Builder[Yes, No, No, No] = {
        ???
      }

      def on(itemId: String)(implicit idEv: ID =:= Yes, prId: PR =:= No): Builder[Yes, Yes, No, No] = {
        ???
      }

      def and(itemId: String)(implicit prId: PR =:= Yes, diEv: DI =:= No): Builder[Yes, Yes, No, No] = {
        ???
      }

      def discounts(percent: Percent)(implicit prId: PR =:= Yes, diEv: DI =:= No): Builder[Yes, Yes, Yes, No] = {
        ???
      }

      @implicitNotFound("Products have not been defined")
      def discounts(cash: Cash)(implicit prId: PR =:= Yes, diEv: DI =:= No): Builder[Yes, Yes, Yes, No] = {
        ???
      }

      def build(implicit idEv: ID =:= Yes, prEv: PR =:= Yes, diEv: DI =:= Yes): Promotion = {
        ???
      }

    }

    def promotion: Builder[No, No, No, No] = new Builder[No, No, No, No] {}

    //Test
    promotion id "abc" on "carrot" and "cheese" discounts 10.percent build
    val promo: Promotion = promotion id "abc" on "carrot" and "cheese" discounts 10.percent build

  }

  val i: 1 = 1

  val value: Map[String, Int] =  List("a" -> 1, "b" -> 2).toMap
  List("a", "b").toMap

}
